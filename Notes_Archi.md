Points historiques
-------------------

<h3> Les premières machines </h3>

* *1642* : **Blaise Pascal** => Première machine à calculer construite avec les additions et soustraction
* *1646* : **Leibnitz** => Ajoute la multipliation et la division à la première machine
* ( *1792 - 1871* ) : **Charles Babbage** =>
    * La machine à différences ( spécifique aux tables de navigation)
    * La machine analytique. Cette machien n'est plus dédiée à une application et les instructions sont lues sur des cartes perforées dont le premier programme a été écrit par Ada Lovelace.


<h3> Les ordinateurs programmables </h3>

* *1938* : Konrad Zuse => invente un ordinateur fonctionnant grâce à **des relais éléctromagnétiques**. ( apparition du binaire )
* *1937* : Howard Aiken => **ordinateur programmable géant**, permettant de calculer 5 fois plus vite que l'homme ( **Le Mark I d'IBM** )
* *1947* : **Le Mark II** voit le jour, ses engrenages sont remplacés par des composants éléctroniques.


<h3> Les ordinateurs à lampes </h3>

* *1946* : **ENIAC** par Mauchley et Eckert. Fonctionne avec des **tubes à vide et des relais**, **plus de 6000 commutateurs**. ( Apparition du **Bug** )

<h3> Les ordinateurs à transistors ( 1955 - 1965 ) </h3>

La course aux **supers ordinateurs** est lancé par la création du transistor.

<h3> Les ordinateurs à circuits intégrés ( 1965 - 1980 ) </h3>

Les dizaines de transistors sont **remplacés par une puce de silicium**. 

<h3> Les ordinateurs personnels ( 1980 - Aujourd'hui ) </h3>

Les circuits intégrés ont permit **des dizaines de milliers, puis des millions de transistors sur des puces** et d'obtenir des ordinateurs toujours plus petits et rapides.

L'ère de l'informatique débute.


Généralités
-------------

*Informatique* : **Science qui traite l'information** , pour se faire elle dispose d'ordinateurs.

*Ordinateur* : Un ordinateur est une machine **capable de résoudres des problèmes en appliquant des instructions préalablement définies**. Un ordinateur ne reconnait à la base qu'un nombre réduit d'instructions, c'est le **langage machine** ( L1 ).

*Langage* : L'ensemble des instructions directement **exécutable par l'ordinateur et qui permet de communiquer avec celle-ci**.

*Le langage machine et le matériel* : sont étroitement liés. On cherche en général un langage simple qui permet de **réduire la complexité et le côut du matériel** ( M1 ).

Concept des machines multicouches
---------------------------------

Au vu de la complexité de manipulation du langage machine (L1 ), il est possible d'imaginer écrire un jeu d'instruction qui serait plus aisé à manipuler. On peut alors créer un nouveau langage ( S2 ).

Pour éxécuter un programme écrit en L2 sur un ordinateur il y a deux solutions : 
* ***Traduire** les instructions de L2 par les suites d'instructions L1 correspondantes.
* Ecrire un programme en L1 permettant d'examiner et comprendre chaque instruction L2 et d'y connecté une instruction L1 correspondante : **nous faisons appel à un interpéteur.**

Une troisième solution serait d'imaginer une nouvelle machine au même niveau que ce langage L2. => une machine virtuelle M2.

De cette manière, _lorsqu'on travaille à un niveau n, nous n'avons pas besoin de nous soucier des niveaux inférieurs_.

<h3>  Machine virtuelle </h3>


L'association d'un matériel n et d'un langage n.

Il existe généralement de nombreuses couches dans les ordinateurs actuels : 

<h4> 5 : La couche des langages d'application </h4>

* C'est le niveau **des langages de haut niveau** tels que Pascal, C/C++ , Ada.. les programmes sont traduits par des compilateurs en langage de niveau 3 ou 4.

<h4> 4 :  La couche des langages d'assemblage </h4>


* C'est à ce niveau que les programmeurs usuels interviennent pour la création de leurs applications. C'est ici que l'ont peut écrire des programmes gérant les niveaux inférieurs. Le traducteur nécessaire à ce langage est appelé **assembleur**.
* Cette couche diffère des autres par le fait qu'elle soit traitée par traduction et non interprétation.
* Les programmes sources sont ici transformés par un traducteur en langage cible. 
* Lorsque le langage source est essentiellement une représentation symbolique du langage d'une machine, le traducteur est appelé **assembleur**.
* Si le langage source est de haut niveau comme C et Pascal, le traducteur est appelé **compilateur**.
    
    

<h4> 3 : La couche du système d'exploitation </h4>


* A ce niveau sont rajoutées les **instructions de gestion machine**
* Le SE peut être vu comme un interpréteur de certaines caractéristiques manquantes dans les couches inférieures, comme la mémoire virtuelle, les instructions d'E/S virtuelles et la possibilité de traitement parallèle.
* La mémoire virtuelle permet d'utiliser un espace d'instruction plus grand que la mémoire physique.
* L'abstraction d'E/S est la plus **importante** car on parle ici de **fichier**. Un fichier est en réalité une suite d'enregistrements logiques.
    

<h4> 2 :  La couche machine traditionnelle </h4>


* La première couche visible par l'utilisateur, elle regroupe toutes les instructions décrites dans les manuels du processeur. Cette couche correspondant à celle du **" langage machine "**.
* La machine dispose à ce niveau d'une mémoire organisée en mots ou octets.
* Les instructions de cette couche permettent **le déplacement des données, l'execution d'opérations arithmétiques et booléennes, des branchements, d'opération spécifiques E/S**.
* Ce sont ces instructions qui sont interprétées par la couche micro programmée.

<h4> 1 : La couche micro programmée  </h4>

*  Un programme appelé **Micro Programme**, formé de quelques instructions de base, controle les composants logiques, analyse et interpète également la couche machine traditionnelle. Ses diverses fonctions définissent **le comportement primaire de l'ordinateur**.
* Architecture CISC / RISC.
    
    
<h4> 0 :  La couche physique </h4>

* Les objets manipulés à ce niveau sont **des composants logiques** formés de transistors, les ordinateurs sont élaborés  à partir d'associations plus ou moins complexes de circuits logiques.
    * Portes, codeurs, multiplexeurs, additionneurs, RAM, ROM, Bus..


Structure d'un ordinateur
----------------------------

<h3> Le processeur / UC </h3>

>**L'UC est le cerveau de l'ordinateur**, elle _exécute_ les programmes stockés en mémoire centrale en chargeant les instructions de ces dernières, puis  _décode_ celles-ci et finalement les _exècutent_ l'une après l'autre.

Elle est composée :
* **D'une unité de commande** qui charge et décode les instructions.
* **D'une unité arithmétique et logique** qui exécute des opérations arithmétiques ( +, - , .. ) et booléennes ( et, ou..).
* **De registres**, qui permettent de stocker les résultats temporaires ou des informations de commande.
    * Compteur Ordinal, Registre d'instructions, accumulateur, registre d'adressage, registre pointeur de pile. 
* **D'une unité de gestion de bus**.
* **D'une horloge**.

<h4> Execution d'une instruction </h4>

Un **programme** est une **suite séquentielle d'instructions.**

**L'ensemble de toutes les instructions** dont dispose le programme a un niveau donné s'appelle **jeu d'instructions.**

Les instructions sont **stockées** dans la **mémoire principale**. Une instruction est composée de deux champs :
* Le code opération : l'action que le processeur doit accomplir.
*  Le codé opérande : le paramètre de l'action. Cela peut être une donnée ou bien une adresse d'un emplacement mémoire.

Un grand nombre d'instructions n'est pas forcément la meilleure solution. Dans certains cas, elle ne seraient pas suffisamment générales ce qui compliqueraient l'écriture des compilateurs et traducteurs, voir même ralentir la machine.

<h4> Le cycle : chargement, décodage et exécution </h4>


| cycle | instruction |
| ------ | ------ |
| 1 | Chargement de la prochaine instruction à exécuter depuis la mémoire jusque dans le registre d'instruction |
| 2 | Modification du compteur ordinal pour qu'il pointe sur l'instruction suivante | 
| 3 | Décodage de l'instruction chargée | 
| 4 | Localisation en mémoire des données utilisées par l'instruction si nécessaire | 
| 5 | Chargement des données dans les registres internes si nécessaires | 
| 6 | Exécution de l instruction | 
| 7 | Exécution de l instruction | 

![cycle] (Screenshot_1.png)


